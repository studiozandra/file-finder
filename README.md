# File Finder

A single-page app for locating physical paper folders amongst file archive racks.
For now I'm keeping the HTML, CSS, and JavaScript in one file -- against standard practice -- for ease of sharing the file with non-technical colleagues via email.